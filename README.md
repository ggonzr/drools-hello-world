# drools-hello-world

This projects implements the Hello World example explained on Drools documentation using Gradle & Maven as central repositories to automatically download all required packages.

If you want more details about drools' schema, please follow the next link: [Drools Schema](https://docs.jboss.org/drools/release/7.49.0.Final/drools-docs/html_single/index.html#decision-examples-helloworld-ref_drools-examples), and navigate through chapter 20.01


